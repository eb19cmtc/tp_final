﻿using System;
using System.Collections.Generic;
using System.Text;

namespace tp_final
{
    class Persona
    {
        string nombre, apellido;
        int edad;
        char sexo;

        public Persona(string nombre, string apellido , int edad , char sexo)
        {
            this.nombre = nombre;
            this.apellido = apellido;
            this.edad = edad;
            this.sexo = sexo;
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }
            set
            {
                nombre = value;
            }
        }

        public string Apellido
        {
            get
            {
                return apellido;
            }
            set
            {
                apellido = value;
            }
        }
        public int Edad
        {
            get
            {
                return edad;
            }
            set
            {
                edad = value;
            }
        }
        public char Sexo
        {
            get
            {
                return sexo;
            }
            set
            {
                sexo = value;
            }
        }

        public override string ToString()
        {
            return "\nNombre : " + nombre + "\nApellido : " + apellido + "\nEdad : " + edad + "\nSexo : " + sexo;
        }
    }
}
