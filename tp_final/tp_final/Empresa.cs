﻿using System;
using System.Collections.Generic;
using System.Text;

namespace tp_final
{
    class Empresa
    {

        string nombre;
        double ganancia;
        List<Area> areas = new List<Area>();
        Ram Rams = new Ram(3000, "Memoria ram", "Hyperx", 100, 8, "DDR4");
        Cpu Cpus = new Cpu(7000, "i8", "Intel", 500, 8.3, 10);
        Fuente Fuentes = new Fuente(5000, "Fuente Gamer full rgb 4k refrigeracion liquida con mercurio", "Gamegax", 1000, 500);

        public Empresa(string nombre)
        {
            this.nombre = nombre;
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }
        }

        public double Ganancia
        {
            get
            {
                return ganancia;
            }
        }

        public void AgregarArea(Area a)
        {
            areas.Add(a);
        }

        public override string ToString()
        {
            return "Empresa " + nombre + "\nGanancia actual:" + ganancia + "\n";
        }

        public void AgregarStockRams(int stock)
        {
            Rams.AumentarStock(stock);
        }
        public void AgregarStockCpus(int stock)
        {
            Cpus.AumentarStock(stock);
        }
        public void AgregarStockFuentes(int stock)
        {
            Fuentes.AumentarStock(stock);
        }

        public void VenderRam(int a)
        {
            Rams.Cantidad = Rams.Cantidad - a;
            ganancia = ganancia + Rams.Precio*a;
        }
        public void VenderCpu(int a)
        {
            Cpus.Cantidad = Cpus.Cantidad - a;
            ganancia = ganancia + Cpus.Precio*a;
        }
        public void VenderFuente(int a)
        {
            Fuentes.Cantidad = Fuentes.Cantidad - a;
            ganancia = ganancia + Fuentes.Precio*a;
        }
    }
}
