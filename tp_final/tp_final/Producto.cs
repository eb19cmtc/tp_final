﻿using System;
using System.Collections.Generic;
using System.Text;

namespace tp_final
{
    abstract class Producto
    {
        double precio;
        string nombre;
        string marca;
        int cantidad;

        public Producto(double precio, string nombre, string marca, int cantidad)
        {
            this.precio = precio;
            this.nombre = nombre;
            this.marca = marca;
            this.cantidad = cantidad;
        }

        public double Precio
        {
            get
            {
                return precio;
            }
            set
            {
                precio = value;
            }
        }
        public string Nombre
        {
            get
            {
                return nombre;
            }
            set
            {
                nombre = value;
            }
        }
        public string Marca
        {
            get
            {
                return marca;
            }
            set
            {
                marca = value;
            }
        }
        public int Cantidad
        {
            get
            {
                return cantidad;
            }
            set
            {
                cantidad = value;
            }
        }

        public override string ToString()
        {
            return "\nPrecio : " + precio + "\nNombre : " + nombre + "\nMarca : " + marca + "\nCantidad : " + cantidad;
        }

        public void AumentarStock(int a)
        {
            cantidad = cantidad + a;
            Console.WriteLine("Cantidad de stock actualizado: " + cantidad);
        }

    }
}
