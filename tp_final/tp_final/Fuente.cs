﻿using System;
using System.Collections.Generic;
using System.Text;

namespace tp_final
{
    class Fuente : Producto
    {

        int watts;

        public Fuente(double precio, string nombre, string marca, int cantidad, int watts) : base(precio, nombre, marca, cantidad)
        {
            this.watts = watts;
        }

        public int Watts
        {
            get
            {
                return watts;
            }
            set
            {
                watts = value;
            }
        }

        public override string ToString()
        {
            return base.ToString() + "\nWatts : " + watts;
        }

    }
}
