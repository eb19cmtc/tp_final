﻿using System;
using System.Collections.Generic;
using System.Text;

namespace tp_final
{
    class Cpu : Producto
    {

        double velocidad;
        int generacion;

        public Cpu(double precio, string nombre, string marca, int cantidad, double velocidad, int generacion) : base(precio, nombre, marca, cantidad)
        {
            this.velocidad = velocidad;
            this.generacion = generacion;
        }

        public double Velocidad
        {
            get
            {
                return velocidad;
            }
            set
            {
                velocidad = value;
            }
        }
        public int Generacion
        {
            get
            {
                return generacion;
            }
            set
            {
                generacion = value;
            }
        }

        public override string ToString()
        {
            return base.ToString() + "\nVelocidad : " + velocidad + "GHz" + "\nGeneración : " + generacion;
        }

    }
}
