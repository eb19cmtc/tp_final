﻿using System;
using System.Collections.Generic;
using System.Text;

namespace tp_final
{
    class Programador : Empleado
    {
        string trabajo;

        public Programador(string nombre, string apellido, int edad, char sexo, double sueldo, int añosServicio) : base(nombre, apellido, edad, sexo,sueldo,añosServicio)
        {
            trabajo = "Programador";
        }
        public string Trabajo
        {
            get
            {
                return trabajo;
            }
            set
            {
                trabajo = value;
            }
        }
        public override string ToString()
        {
            return base.ToString() + "\nTrabajo : " + trabajo +"\n";
        }
    }
}
