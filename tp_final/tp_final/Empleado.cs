﻿using System;
using System.Collections.Generic;
using System.Text;

namespace tp_final
{
    class Empleado : Persona
    {
        double sueldo;
        int añosServicio;

        public Empleado(string nombre, string apellido, int edad, char sexo,double sueldo,int añosServicio) : base(nombre, apellido, edad, sexo)
        {
            this.sueldo = sueldo;
            this.añosServicio = añosServicio;
        }
        public double Sueldo
        {
            get
            {
                return sueldo;
            }
            set
            {
                sueldo = value;
            }
        }
        public int AñosServicio
        {
            get
            {
                return añosServicio;
            }
            set
            {
                añosServicio = value;
            }
        }
        public override string ToString()
        {
            return base.ToString() + "\nSueldo : " + sueldo + "\nAños de servicio : " + añosServicio;
        }
        public void AumentoSalario(double a)
        {
            sueldo = sueldo + a;
        }
    }
}
