﻿using System;
using System.Collections.Generic;

namespace tp_final
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Empleado> lista_empleados = new List<Empleado>();
            Area area1 = new Area(1);
            Area area2 = new Area(2);
            Area area3 = new Area(3);
            bool repetir = true;
            int switch_int;
            int regreso;
            string nombreEmpresa;

            Console.WriteLine("Escribir nombre de la empresa:");
            nombreEmpresa = Console.ReadLine();
            Empresa empresa = new Empresa(nombreEmpresa);

            empresa.AgregarArea(area1);
            empresa.AgregarArea(area2);
            empresa.AgregarArea(area3);

            Console.Clear();

            while (repetir)
            {
                Console.WriteLine("Empresa " + empresa.Nombre);
                Console.WriteLine("");
                Console.WriteLine("1 Contratar empleados");
                Console.WriteLine("2 Monstrar empleados por area");
                Console.WriteLine("3 Monstrar a todos los empleados");
                Console.WriteLine("4 Aumentar el salario a un empleado especifico");
                Console.WriteLine("5 Aumentar el salario a todos los empleados de una area");
                Console.WriteLine("6 Aumentar el salario a todos los empleados");
                Console.WriteLine("7 Aumentar el salario a los empleados que tengan mas de x años de servicio");
                Console.WriteLine("8 Despedir un empleado");
                Console.WriteLine("9 Aumentar el stock de un determinado producto");
                Console.WriteLine("10 Vender un producto");
                Console.WriteLine("11 Calcular ganancias de la empresa");

                switch_int = int.Parse(Console.ReadLine());
                Console.Clear();
                switch (switch_int)
                {
                    case 1:

                        int cont;
                        Console.WriteLine("Progamador 1 , Diseñador 2, Administrador 3 ");
                        cont = int.Parse(Console.ReadLine());

                        string nombre, apellido;
                        int edad, añosServicio;
                        double sueldo;
                        char sexo;

                        Console.WriteLine("Nombre");
                        nombre = Console.ReadLine();
                        Console.WriteLine("Apellido");
                        apellido = Console.ReadLine();
                        Console.WriteLine("Edad");
                        edad = int.Parse(Console.ReadLine());
                        Console.WriteLine("Sexo H o M");
                        sexo = char.Parse(Console.ReadLine());
                        Console.WriteLine("Sueldo");
                        sueldo = double.Parse(Console.ReadLine());
                        Console.WriteLine("Años de servicio");
                        añosServicio = int.Parse(Console.ReadLine());

                        int area;
                        Console.WriteLine("Area a asignar del 1 al 3");
                        area = int.Parse(Console.ReadLine());
                        switch (area)
                        {
                            case 1:
                                switch (cont)
                                {
                                    case 1:
                                        Programador trabajadorP = new Programador(nombre, apellido, edad, sexo, sueldo, añosServicio);
                                        Console.WriteLine(trabajadorP);
                                        lista_empleados.Add(trabajadorP);
                                        area1.agregarEmpleado(trabajadorP);
                                        break;
                                    case 2:
                                        Diseñador trabajadorD = new Diseñador(nombre, apellido, edad, sexo, sueldo, añosServicio);
                                        Console.WriteLine(trabajadorD);
                                        lista_empleados.Add(trabajadorD);
                                        area1.agregarEmpleado(trabajadorD);
                                        break;
                                    case 3:
                                        Administrador trabajadorA = new Administrador(nombre, apellido, edad, sexo, sueldo, añosServicio);
                                        Console.WriteLine(trabajadorA);
                                        lista_empleados.Add(trabajadorA);
                                        area1.agregarEmpleado(trabajadorA);
                                        break;
                                    default:
                                        Console.WriteLine("Ingresar un numero indicado");
                                        break;
                                }
                                break;
                            case 2:
                                switch (cont)
                                {
                                    case 1:
                                        Programador trabajadorP = new Programador(nombre, apellido, edad, sexo, sueldo, añosServicio);
                                        Console.WriteLine(trabajadorP);
                                        lista_empleados.Add(trabajadorP);
                                        area2.agregarEmpleado(trabajadorP);
                                        break;
                                    case 2:
                                        Diseñador trabajadorD = new Diseñador(nombre, apellido, edad, sexo, sueldo, añosServicio);
                                        Console.WriteLine(trabajadorD);
                                        lista_empleados.Add(trabajadorD);
                                        area2.agregarEmpleado(trabajadorD);
                                        break;
                                    case 3:
                                        Administrador trabajadorA = new Administrador(nombre, apellido, edad, sexo, sueldo, añosServicio);
                                        Console.WriteLine(trabajadorA);
                                        lista_empleados.Add(trabajadorA);
                                        area2.agregarEmpleado(trabajadorA);
                                        break;
                                    default:
                                        Console.WriteLine("Ingresar un numero indicado");
                                        break;
                                }
                                break;
                            case 3:
                                switch (cont)
                                {
                                    case 1:
                                        Programador trabajadorP = new Programador(nombre, apellido, edad, sexo, sueldo, añosServicio);
                                        Console.WriteLine(trabajadorP);
                                        lista_empleados.Add(trabajadorP);
                                        area3.agregarEmpleado(trabajadorP);
                                        break;
                                    case 2:
                                        Diseñador trabajadorD = new Diseñador(nombre, apellido, edad, sexo, sueldo, añosServicio);
                                        Console.WriteLine(trabajadorD);
                                        lista_empleados.Add(trabajadorD);
                                        area3.agregarEmpleado(trabajadorD);
                                        break;
                                    case 3:
                                        Administrador trabajadorA = new Administrador(nombre, apellido, edad, sexo, sueldo, añosServicio);
                                        Console.WriteLine(trabajadorA);
                                        lista_empleados.Add(trabajadorA);
                                        area3.agregarEmpleado(trabajadorA);
                                        break;
                                    default:
                                        Console.WriteLine("Ingresar un numero indicado");
                                        break;
                                }
                                break;
                            default:
                                Console.WriteLine("Escribir un numero que se indique");
                                break;
                        }
                        break;

                    case 2:

                        int areas;
                        Console.WriteLine("Elegir del 1 al 3 el numero del area");
                        areas = int.Parse(Console.ReadLine());
                        switch (areas)
                        {
                            case 1:
                                area1.asignarJefe();
                                Console.WriteLine(area1);
                                break;
                            case 2:
                                area2.asignarJefe();
                                Console.WriteLine(area2);
                                break;
                            case 3:
                                area3.asignarJefe();
                                Console.WriteLine(area3);
                                break;
                            default:
                                Console.WriteLine("Ingrese un numero correcto");
                                break;
                        }
                        break;

                    case 3:

                        foreach (Empleado i in lista_empleados)
                        {
                            Console.WriteLine(i);
                            Console.WriteLine("---------------------------------");
                        }
                        break;
                    case 4:

                        int contador = 0;
                        int empleado;
                        double aumento;

                        foreach (Empleado i in lista_empleados)
                        {
                            Console.WriteLine(contador + " " + i.Nombre + " " + i.Apellido);
                            contador++;
                            Console.WriteLine(" ");
                        }

                        Console.WriteLine("Elija el numero del empleado para aumenar su salario:");
                        empleado = int.Parse(Console.ReadLine());

                        Console.WriteLine("Ingrese el valor a aumentar en el salario de dicho empleado:");
                        aumento = double.Parse(Console.ReadLine());
                        lista_empleados[empleado].AumentoSalario(aumento);
                        Console.WriteLine("Sueldo actualizado: " + lista_empleados[empleado].Sueldo);

                        break;
                    case 5:

                        int nro_area;
                        double aumento2;

                        Console.WriteLine("Ingrese el valor a aumentar en los salarios");
                        aumento2 = double.Parse(Console.ReadLine());

                        Console.WriteLine("Seleccionar Area para aumentar el sueldo de los empleados residentes de dicha area");
                        nro_area = int.Parse(Console.ReadLine());

                        switch (nro_area)
                        {
                            case 1:

                                List<Empleado> guardarLista = area1.EmpleadosEnArea();

                                foreach (Empleado i in guardarLista)
                                {

                                    i.AumentoSalario(aumento2);

                                }

                                break;

                            case 2:

                                List<Empleado> guardarLista2 = area2.EmpleadosEnArea();

                                foreach (Empleado i in guardarLista2)
                                {

                                    i.AumentoSalario(aumento2);

                                }

                                break;

                            case 3:

                                List<Empleado> guardarLista3 = area3.EmpleadosEnArea();

                                foreach (Empleado i in guardarLista3)
                                {

                                    i.AumentoSalario(aumento2);

                                }

                                break;

                            default:

                                Console.WriteLine("Ingrese Area Valida");

                                break;
                        }

                        break;

                    case 6:

                        double aumento3;

                        Console.WriteLine("Ingresar la cantidad para aumentar en los salarios de todos los empleados de la empresa:");
                        aumento3 = double.Parse(Console.ReadLine());

                        foreach (Empleado i in lista_empleados)
                        {

                            i.AumentoSalario(aumento3);

                        }

                        break;

                    case 7:

                        int años;
                        double aumento4;
                        Console.WriteLine("Ingresar cantidad de aumento X años");
                        años = int.Parse(Console.ReadLine());

                        Console.WriteLine("Cantidad a aumentar");
                        aumento4 = double.Parse(Console.ReadLine());

                        foreach(Empleado i in lista_empleados)
                        {
                            int a = i.AñosServicio;
                            int b = a / años;

                            if(b>= 1)
                            {
                                i.AumentoSalario(b * aumento4);
                            }
                            else
                            {

                            }
                            
                        }


                        break;

                    case 8:

                        int contador1 = 0;
                        int empleado1;
                        

                        foreach (Empleado i in lista_empleados)
                        {
                            Console.WriteLine(contador1 + " " + i.Nombre + " " + i.Apellido);
                            contador1++;
                            Console.WriteLine(" ");
                        }

                        Console.WriteLine("Elija el numero del empleado que quiere despedir");
                        empleado1 = int.Parse(Console.ReadLine());

                        lista_empleados.RemoveAt(empleado1);
                        Console.WriteLine("El empleado ha sido removido");
                        break;

                    case 9:

                        int cont1;
                        int stock;

                        Console.WriteLine("Seleccionar producto para aumentar stock:");
                        Console.WriteLine("Rams 1, Cpus 2, Fuentes 3");
                        cont1 = int.Parse(Console.ReadLine());

                        switch (cont1)
                        {
                            case 1:

                                Console.WriteLine("Ingresar la cantidad de stock que desea agregar:");
                                stock = int.Parse(Console.ReadLine());
                                empresa.AgregarStockRams(stock);

                                break;

                            case 2:

                                Console.WriteLine("Ingresar la cantidad de stock que desea agregar:");
                                stock = int.Parse(Console.ReadLine());
                                empresa.AgregarStockCpus(stock);

                                break;

                            case 3:

                                Console.WriteLine("Ingresar la cantidad de stock que desea agregar:");
                                stock = int.Parse(Console.ReadLine());
                                empresa.AgregarStockFuentes(stock);

                                break;

                            default:

                                Console.WriteLine("Ingresar numero del 1 al 3");

                                break;

                        }

                        break;

                    case 10:

                        int cont2;
                        int venta;

                        Console.WriteLine("Seleccionar producto a vender:");
                        Console.WriteLine("Rams 1, Cpus 2, Fuentes 3");
                        cont2 = int.Parse(Console.ReadLine());

                        switch (cont2)
                        {
                            case 1:

                                Console.WriteLine("Ingresar la cantidad que desea vender:");
                                venta = int.Parse(Console.ReadLine());
                                empresa.VenderRam(venta);

                                break;

                            case 2:

                                Console.WriteLine("Ingresar la cantidad que desea vender:");
                                venta = int.Parse(Console.ReadLine());
                                empresa.VenderCpu(venta);

                                break;

                            case 3:

                                Console.WriteLine("Ingresar la cantidad que desea vender:");
                                venta = int.Parse(Console.ReadLine());
                                empresa.VenderFuente(venta);

                                break;

                            default:

                                Console.WriteLine("Ingresar numero del 1 al 3");

                                break;

                        }

                        break;

                    case 11:

                        Console.WriteLine("Ganancia de la empresa actualmente: " + empresa.Ganancia);

                        break;

                    default:
                        Console.WriteLine("Escribir un numero respecto al menu");
                        break;
                }

                Console.WriteLine("");
                Console.WriteLine("Regresar al menu 1 , salir 0");
                regreso = int.Parse(Console.ReadLine());
                if (regreso == 0)
                {
                    repetir = false;
                }
                Console.Clear();



            }

            


        }
    }
}
