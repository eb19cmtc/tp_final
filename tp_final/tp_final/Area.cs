﻿using System;
using System.Collections.Generic;
using System.Text;

namespace tp_final
{
    class Area
    {
        List<Empleado> lista_empleados = new List<Empleado>();
        Empleado jefe_area;
        int numeroArea;

        public Area(int numeroArea)
        {
            this.numeroArea = numeroArea;
        }
        public void asignarJefe()
        {
            Random rnd = new Random();
            int i = rnd.Next(0,lista_empleados.Count - 1);

            jefe_area = lista_empleados[i];
        }
        public void agregarEmpleado(Empleado a)
        {
            lista_empleados.Add(a);
        }
        public int NumeroArea 
        {
            get
            {
                return numeroArea;
            }
            set
            {
                numeroArea = value;
            }
        }
        public override string ToString()
        {
            Console.WriteLine("Jefe de area ");

            Console.WriteLine(jefe_area);
            Console.WriteLine();
            Console.WriteLine("---------------------");
            string guardar = "";
            foreach(Empleado a in lista_empleados)
            {
                guardar = guardar + "Empleado : \n " + a.ToString() + "\n";
            }
            return guardar;
        }
        
        public List<Empleado> EmpleadosEnArea()
        {
            return lista_empleados;
        }

    }
}
