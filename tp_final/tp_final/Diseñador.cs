﻿using System;
using System.Collections.Generic;
using System.Text;

namespace tp_final
{
    class Diseñador : Empleado
    {
        string trabajo;

        public Diseñador(string nombre, string apellido, int edad, char sexo, double sueldo, int añosServicio) : base(nombre, apellido, edad, sexo, sueldo, añosServicio)
        {
            trabajo = "Diseñador";
        }
        public string Trabajo
        {
            get
            {
                return trabajo;
            }
            set
            {
                trabajo = value;
            }
        }
        public override string ToString()
        {
            return base.ToString() + "\nTrabajo : " + trabajo + "\n";
        }
    }
}
