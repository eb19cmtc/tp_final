﻿using System;
using System.Collections.Generic;
using System.Text;

namespace tp_final
{
    class Ram : Producto
    {
        int gigabytes;
        string tecnologia;

        public Ram(double precio, string nombre, string marca, int cantidad, int gigabytes, string tecnologia) : base(precio, nombre, marca, cantidad)
        {
            this.gigabytes = gigabytes;
            this.tecnologia = tecnologia;
        }

        public int Gigabytes
        {
            get
            {
                return gigabytes;
            }
            set
            {
                gigabytes = value;
            }
        }
        public string Tecnologia
        {
            get
            {
                return tecnologia;
            }
            set
            {
                tecnologia = value;
            }
        }

        public override string ToString()
        {
            return base.ToString() + "\nGigabytes : " + gigabytes + "\nTecnología : " + tecnologia;
        }

    }
}
